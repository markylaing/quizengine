# quizengine

##### Run
```bash
git clone git@gitlab.com:markylaing/quizengine.git
cd quizengine
docker-compose up
```

##### Develop
```bash
git clone git@gitlab.com:markylaing/quizengine.git
cd quizengine
virtualenv -p python3.6 venv
source venv/bin/activate
pip install -r requirements.txt
./db/dev-env.sh # Starts postgres in a container and migrates the schema
python django_school/manage.py runserver
```

### Approach

I initially searched for frameworks with which I could bootstrap the project but couldn't find anything either a) with enough features to fulfil requirements, or b) in a language I feel comfortable with.

I then searched for open-source implementations and found the django school project which approximately meets requirements.

### Initial steps
1. Modified the `django_school` project to report the percentage of users who performed within the same percentile.
2. Modifies the `django_school` project to use a postgres database (so that this can be queried by the go script).
3. Containerised `django_school` for later use with docker/kubernetes.
4. Created `reporter/main.go` to query the database and send emails to the admins with quizzes and scores (in percentiles). Given that this is a short script I didn't see the need to search for third-party software/frameworks.

### Next steps
1. Containerise the golang script.
2. Push containers to a docker repo.
3. Create helm charts for the project:
    * Deploy the `reporter` as a kubernetes [CronJob](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/).
    * Deploy `django_school` to kubernetes as a service.
    * Either deploy postgres to a VM or kubernetes (depending on whether k8s has persistent volume facility). Use a managed solution if possible.
4. Further modify the `django_school` project so that it is more fit for purpose:
    * Remove requirement for "students" to login and ask for email at the end of a test.
    * Remove subject/categories.
    * Enforce number of questions to equal 4.
5. Address tech-debt:
    * Many instances of credentials and config within the codebase. Credentials should be stored in a protected key store and much of the config should be extracted to environment variables with defaults.
    * CI/CD needs to be implemented with appropriate unit/integration tests. 
