package main

import (
	"bytes"
	"fmt"
	"html/template"
	"net/smtp"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

const (
	senderEmail = "quizbot@example.com"
	senderPassword = "password"
	mailHost = "mail.example.com:25"
)

type user struct {
	Id    int    `db:"id"`
	Email string `db:"username"`
}

type result struct {
	QuizId int `db:"id"`
	QuizName string `db:"name"`
	Score float32 `db:"score"`
}

var t = template.Must(template.New("").Parse(`
{{ range $quizName, $percentiles := . }}
<h2>{{ $quizName }}</h2>
<p>Total attempts: {{ index $percentiles -1 }}</p>
<ul>{{ range $percentile, $count := $percentiles}}
	<li>{{ if ne $percentile -1 }}{{ $percentile }}0th percentile: {{ $count }}{{ end }}</li>
{{ end }}</ul>
{{ end }}
`))

var auth = smtp.PlainAuth("", senderEmail, senderPassword, mailHost)

func main() {

	db := sqlx.MustConnect("postgres", "user=postgres dbname=quizengine password=admin host=localhost port=5432 sslmode=disable")

	var users []user
	if err := db.Select(&users, `SELECT username, id FROM classroom_user WHERE is_teacher = 't'`); err != nil {
		fmt.Printf("%+v", err)
		panic("could not query database")
	}

	for _, user := range users {
		var results []result
		if err := db.Select(&results, fmt.Sprintf(
			`SELECT q.id, q.name, t.score 
					FROM classroom_quiz AS q 
					JOIN classroom_takenquiz AS t 
					ON q.id = t.quiz_id 
					WHERE q.owner_id = %d`, user.Id),
		); err != nil {
			fmt.Printf("%+v", err)
			panic("could not query database")
		}

		resultSummary := make(map[string]map[int]int)
		for _, result := range results {
			_, ok := resultSummary[result.QuizName]
			if !ok {
				resultSummary[result.QuizName] = make(map[int]int)
			}
			percentile := int(result.Score / 10)
			if percentile == 10 {percentile--} // Percentiles should be 0 to 9
			resultSummary[result.QuizName][percentile]++
			resultSummary[result.QuizName][-1]++ // Use -1 to signify sum
		}

		var buffer bytes.Buffer
		if err := t.Execute(&buffer, resultSummary); err != nil {
			panic("could not render email template")
		}

		fmt.Println(buffer.String())

		if err := smtp.SendMail(mailHost, auth, senderEmail, []string{user.Email}, buffer.Bytes()); err != nil {
			fmt.Printf("Failed to send mail to quiz admin: %+v", err)
		}
	}
}
